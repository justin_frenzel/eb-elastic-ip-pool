'use strict';

var ipPool = [
	'eipalloc-d6edc5ea'
];

(async () => {
	await require('./index.js')(true, ipPool);
	console.log("All done!");
	process.exit();
})();
