'use strict';

const aws = require('aws-sdk');
const os = require('os');

aws.config.update({region: process.env.AWS_DEFAULT_REGION || 'us-west-2'});
const _ec2 = new aws.EC2();

//This will ensure the box has an elastic IP associated.
//It will return true if an address has to be associated, null if no action needed to be taken, or false if the ip failed to associate.
async function ElasticBeanstalkElasticIPPool (infoLog = true, elasticIpPool = null) {
	try {
		if (os.hostname().indexOf("ip-") === -1) {
			if (infoLog)
				console.log("This machine is not an EC2 box, no elastic IP assignment is needed.");
			return null;
		}

		if (!elasticIpPool) {
			if (!process.env.ELASTIC_IP_POOL)
				throw new Error(`There is no elastic IP pool configured for this instance to use!`);
			elasticIpPool = process.env.ELASTIC_IP_POOL.split(',');
		}

		var instance = await getInstance();
		var addresses = await getElasticIps(elasticIpPool);

		if (instanceHasElasticIpAssociated(instance, addresses))
			return null;

		await associateElasticIpToInstanceFromPool(instance, addresses);
		return true;
	}
	catch (err) {
		if (infoLog) {
			console.error(err);
			console.error(new Error(`Unable to ensure that this instance has an elastic IP!`));
		}
		return false;
	}
}

async function getInstance (instanceId = null) {
	return new Promise((resolve, reject) => {
		_ec2.describeInstances((err, i) => {
			if (err)
				return reject(err);

			let host = os.hostname();
			host = ensureHostnameHasDecimalIP(host);

			for (let group of i.Reservations) {
				for (let instance of group.Instances) {
					//console.log(`is ${host} == ${instance.PrivateDnsName.split(".")[0]}: ${JSON.stringify((host == instance.PrivateDnsName.split(".")[0]))}`);
					if (host == instance.PrivateDnsName.split(".")[0])
						return resolve(instance);
					if (instanceId && instance.InstanceId == instanceId)
						return resolve(instance);
					if (instance.PrivateDnsName.includes(host))
						return resolve(instance);
				}					
			}

			return reject(new Error(`Could not locate an AWS instance referring to this machine '${instanceId || os.hostname()}'.`))
		});
	});
}

async function getElasticIps (pool) {
	return new Promise((resolve, reject) => {
		_ec2.describeAddresses((err, r) => {
			if (err)
				return reject(err);

			var fromMyPool = [];

			for (let address of r.Addresses) {
				//my pool contains this address
				if (pool.indexOf(address.AllocationId) !== -1)
					fromMyPool.push(address);
			}
	
			return resolve(fromMyPool);
		});
	});
}

function instanceHasElasticIpAssociated (instance, addresses) {
	for (let address of addresses) {
		if (address.InstanceId == instance.InstanceId) {
			console.log(`Instance Id '${instance.InstanceId}' already has Elastic IP '${address.PublicIp}' associated.`);
			return true;
		}
	}

	return false;
}

async function associateElasticIpToInstanceFromPool (instance, addresses) {
	return new Promise((resolve, reject) => {
		var atLeastOneAvailableAddress = false;
		for (let address of addresses) {
			if (typeof address.InstanceId == "undefined") {
				atLeastOneAvailableAddress = true;
				_ec2.associateAddress({
					AllocationId: address.AllocationId,
					InstanceId: instance.InstanceId,
					AllowReassociation: false,
					DryRun: false
				}, (err, result) => {
					console.log(`Successfully associated Elastic Ip '${address.PublicIp}' to instance '${instance.InstanceId}'!`)
					return (err) ? reject(err) : resolve(true);
				});
				break;
			}
		}

		if (atLeastOneAvailableAddress == false)
			return reject(new Error(`There are no available Elastic IPs to associate to this instance from your pool!`));
	})
}

function hostnameHasHexIP(hostname) {
    // Regular expression to find an 8-character hexadecimal string
    const hexIpPattern = /([0-9a-fA-F]{8})/;
    return hostname.match(hexIpPattern);
}

function hexToDecIp(hexString) {
    // Split the hex string into parts
    const parts = hexString.match(/.{1,2}/g);
    if (!parts || parts.length !== 4) {
        throw new Error('Invalid hexadecimal IP address');
    }

    // Convert each part to a decimal number
    const decimalParts = parts.map(part => parseInt(part, 16));
    return decimalParts.join('-');
}

function ensureHostnameHasDecimalIP(hostname) {
    let match = hostnameHasHexIP(hostname);
    if (match) {
        try {
            const hexIp = match[1];
            const ip = hexToDecIp(hexIp);
            // Replace the hexadecimal IP portion with the converted IP
            const updatedHostname = hostname.replace(hexIp, ip);
            return updatedHostname;
        } catch (error) {
            console.error('Error converting hostname to IP:', error.message);
        }
    }

	return hostname;
}

module.exports = ElasticBeanstalkElasticIPPool;